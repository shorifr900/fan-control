#!/usr/bin/env python3

import argparse
import threading
import time
import sys

from periphery import GPIO

# https://www.instructables.com/id/PWM-Regulated-Fan-Based-on-CPU-Temperature-for-Ras/
# https://www.espruino.com/Software+PWM

def setup_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--pin-number", default=21,
        help="Pin number for the GPIO pin which controls the fan.")
    parser.add_argument("-f", "--frequency", default=100, type=int,
        help="Frequency in Hertz of the PWM duty cycle")
    parser.add_argument("--temperature-path",
        help="Path to the sysfs temperature file."
        "The default is that of a raspberry pi model 3 running arch linux arm64.",
        default="/sys/class/thermal/thermal_zone0/temp")
    parser.add_argument("--gpiochip", default="/dev/gpiochip0",
        help="Device with which to control the GPIO pins (chardev).")
    parser.add_argument("--min-temperature", default=40, type=int,
        help="Turn fan on when temperature reaches this level")
    parser.add_argument("--max-temperature", default=70, type=int,
        help="Turn fan to maximum when temperature reaches this level")
    return parser.parse_args()

class PWM(object):
    def __init__(self, pin_number, frequency=100, duty_cycle=100,
            gpiochip="/dev/gpiochip0"):
        self.pin = GPIO(gpiochip, pin_number, "out")
        self.frequency = frequency
        self.duty_cycle = duty_cycle
        self.activity_on = 0
        self.activity_off = 0

    def start(self):
        self.is_running = True
        while self.is_running:
            try:
                self.pin.write(True)
                time.sleep(self.activity_on)
                self.pin.write(False)
                time.sleep(self.activity_off)
            except KeyboardInterrupt:
                print("interrupted")
                self.is_running = False

    def set_cycle(self, duty_cycle):
        # The duty cycle is specified in percentages so convert to a decimal
        proportion = duty_cycle / 100
        # Frequency is specified in Hz and since the python time.sleep
        # method takes input in seconds, we divide by one to get the total
        # amount of time to be used by a cycle.
        interval = 1 / self.frequency
        self.activity_on = proportion * interval
        self.activity_off = interval - self.activity_on

    def close(self):
        self.pin.close()

def read_temperature(temperature_path):
    try:
        with open(temperature_path) as fp:
            return int(fp.read()) / 1000
    except (FileNotFoundError, ValueError) as e:
        print(f"Error reading temperature: {e}", file=sys.stderr)
        return 0

def linear_conversion(temperature, min_temp=40, max_temp=70):
    # This converts a temperature reading in the scale between 40 and 70 to a value between 0 and 100
    # https://stackoverflow.com/a/929107
    if temperature < min_temp:
        return 0
    elif temperature > max_temp:
        return 100
    return (((temperature - min_temp) * (100 - 0)) / (max_temp - min_temp))

def main():
    args = setup_args()
    p = PWM(args.pin_number, args.frequency, gpiochip=args.gpiochip)
    t = threading.Thread(target=p.start, daemon=True)
    t.start()
    while 1:
        try:
            temperature = read_temperature(args.temperature_path)
            cycle = linear_conversion(temperature, args.min_temperature,
                args.max_temperature)
            print(f"Temperature: {temperature}. Setting cycle: {cycle}")
            p.set_cycle(cycle)
            time.sleep(1)
        except KeyboardInterrupt:
            print("Interrupted")
            break
    p.close()

if __name__ == "__main__":
    main()
